<?php

declare(strict_types = 1);

namespace App\Charts;

use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CovidChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $response = Http::get('https://api.kawalcorona.com/indonesia/provinsi')->json();
        
        $label = array();
        $kasus = array();
        $i=0;

        foreach ($response as $value) {
            array_push($label, $value['attributes']['Provinsi']);
            array_push($kasus, $value['attributes']['Kasus_Posi']);

            $i++;
            if ($i==5) break;
        }

        return Chartisan::build()
            ->labels($label)
            ->dataset('Positif', $kasus);
    }
}