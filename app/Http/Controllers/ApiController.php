<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class ApiController extends Controller
{
    public function index()
    {
        $response = Http::get('https://api.kawalcorona.com/indonesia')->json();
        
        return view('welcome', compact('response'));
    }

    public function provinsi()
    {
        $response = Http::get('https://api.kawalcorona.com/indonesia/provinsi')->json();
        
        return view('provinsi', compact('response'));
    }

    public function chart()
    {
        return view('kasusProvinsiChart');
    }
}
