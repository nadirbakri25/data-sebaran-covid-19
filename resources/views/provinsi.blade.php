@extends('layouts.main')

@section('content')

<table class="table mt-5">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Provinsi</th>
            <th scope="col">Kasus Positif</th>
            <th scope="col">Kasus Sembuh</th>
            <th scope="col">Kasus Meninggal</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($response as $item)
        <tr>
            <th scope="row">{{ $loop->iteration }}</th>
            <td>{{ $item['attributes']['Provinsi'] }}</td>
            <td>{{ $item['attributes']['Kasus_Posi'] }}</td>
            <td>{{ $item['attributes']['Kasus_Semb'] }}</td>
            <td>{{ $item['attributes']['Kasus_Meni'] }}</td>
        </tr>            
        @endforeach
    </tbody>
</table>
  
@endsection