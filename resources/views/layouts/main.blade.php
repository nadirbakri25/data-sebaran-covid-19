<!doctype html>
<html lang="en">
    <head>
        <title>Kawal Corona API</title>
        <link href="/css/app.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="/">Kawal Corona</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-link" href="/">Data Indonesia</a>
                        <a class="nav-link" href="/provinsi">Data Provinsi</a>
                        <a class="nav-link" href="/chart">Chart Provinsi</a>
                    </div>
                </div>
            </div>
        </nav>
        
        <div class="container">
            @yield('content')
        </div>

        <script src="/js/app.js"></script>
    </body>
</html>
