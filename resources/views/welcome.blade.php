@extends('layouts.main')

@section('content')

    <div class="text-center mt-5 pt-5">
        <img class="w-25" src="https://upload.wikimedia.org/wikipedia/commons/9/9f/Flag_of_Indonesia.svg" alt="">
    </div>

    <div class="row justify-content-center mt-5">
        @foreach ($response as $item)
            <div class="col-3" style="border-right: 1px solid black;border-left:1px solid black;">
                <h5>POSITIF</h5>
                <span style="font-size: 40px">{{ $item['positif'] }}</span>
            </div>
            <div class="col-3" style="border-right: 1px solid black;">
                <h5>SEMBUH</h5>
                <span style="font-size: 40px">{{ $item['sembuh'] }}</span>
            </div>
            <div class="col-3" style="border-right: 1px solid black;">
                <h5>MENINGGAL</h5>
                <span style="font-size: 40px">{{ $item['meninggal'] }}</span>
            </div>
            <div class="col-3" style="border-right: 1px solid black;">
                <h5>DIRAWAT</h5>
                <span style="font-size: 40px">{{ $item['dirawat'] }}</span>
            </div>
        @endforeach
    </div>
    
@endsection