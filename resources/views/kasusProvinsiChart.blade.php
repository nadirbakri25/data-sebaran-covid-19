@extends('layouts.main')

@section('content')


<div class="text-center mt-5">
    <h1>TOP 5 KASUS TERBANYAK DI INDONESIA</h1>
    <div id="chart" style="height: 400px;"></div>
</div>





<!-- Charting library -->
<script src="https://unpkg.com/echarts/dist/echarts.min.js"></script>
<!-- Chartisan -->
<script src="https://unpkg.com/@chartisan/echarts/dist/chartisan_echarts.js"></script>

<script>
  const chart = new Chartisan({
    el: '#chart',
    url: "@chart('covid_chart')",
  });
</script>
  
@endsection